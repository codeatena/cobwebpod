package com.cobwebb.mobile.pod.collector;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ManualBarcodeCapture extends Activity {
	
	Button mBack, mSave;
	private EditText barcodeData;
	
	Bundle m_bundle;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		m_bundle = getIntent().getExtras();		
		
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.manual_barcode);
			
		barcodeData = (EditText) findViewById(R.id.manualBarcodeData);
		mSave = (Button) findViewById(R.id.save);
		mBack = (Button) findViewById(R.id.back);
			
		mSave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Signeer Captured Saved");	
				Intent i = getIntent();
				if (barcodeData.getText().toString().trim().equalsIgnoreCase("")) 
				{														
					Toast toast = Toast.makeText(ManualBarcodeCapture.this, "Please enter your Name", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.TOP, 105, 50);
					toast.show();
				}
				else
				{
					//DatabaseHandler db = new DatabaseHandler(SigneeCapture.this);
					
					String sBarcodeData = barcodeData.getText().toString();
					
					if(m_bundle != null)
					{
						//String barcode = m_bundle.getString("barcode"); 
						//String imageUrl = m_bundle.getString("imageUrl");
						m_bundle.putString("barcodeData", sBarcodeData);
						i.putExtras(m_bundle);
					}
											
					setResult(RESULT_OK,  i);
					finish();
				}
			}
		});			
		
		mBack.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Manual Barcode Canceled");				
				setResult(RESULT_CANCELED,  getIntent());
				finish();
			}
		});

	}

	@Override
	protected void onDestroy() {
		Log.w("GetSgnee", "onDestory");
		super.onDestroy();
	}	

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}
}
