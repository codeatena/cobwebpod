package com.cobwebb.mobile.pod.collector;


import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordDlg extends Activity {
	private static final String TAG = "PasswordDlg";
	public static final String PARM_SUCCESS_ACTIVITY = "successActivityClassName";

	EditText password;
	Button ok, cancel;
	String m_successActivityClassName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password);
		
		m_successActivityClassName = "";
	    Bundle extras = getIntent().getExtras();
	    if (extras != null)
	    	m_successActivityClassName = extras.getString(PARM_SUCCESS_ACTIVITY);
	    
		ok = (Button)findViewById(R.id.ok);
		cancel = (Button)findViewById(R.id.cancel);
		password = (EditText)findViewById(R.id.password);
		
	
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
											
				String pass = password.getText().toString().trim();
				
				String requiredPass = "cobpod";
				if(Utils.isDebuggable(getApplicationContext()) == true)
					requiredPass = "";
				if(!pass.equalsIgnoreCase(requiredPass))
				{
					Toast.makeText(PasswordDlg.this, "Password is incorrect.", Toast.LENGTH_SHORT).show();
				}
				else
				{															
					finish();
					try {
						Class<?> cls = Class.forName(m_successActivityClassName);
				        Intent i = new Intent(PasswordDlg.this, cls);
						startActivity(i);
					} catch (ClassNotFoundException e) {
						Log.w(TAG, "Failed to find class, no Intent created");
					}
				}											
			}
		});		
	
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
				
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}
}
