package com.cobwebb.mobile.pod.collector;

import java.util.Locale;

import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AdminActivity extends Activity {
	private static final String TAG = "AdminActivity";
	public static final int UPLOAD_ACTIVITY = 51;

	Button m_btnUploadPendingPODs;
	Button m_btnDeletePendingPODs;
	Button m_btnConfiguration;

	DatabaseHandler m_db = null;
	ConnectionDetector m_cd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admin);
		
		m_db = new DatabaseHandler(this);
		m_cd = new ConnectionDetector(getApplicationContext());

		m_btnUploadPendingPODs 	= (Button) findViewById(R.id.upload_pending_pods);
		m_btnDeletePendingPODs 	= (Button) findViewById(R.id.delete_pending_pods);
		m_btnConfiguration			= (Button) findViewById(R.id.configuration);
		
		//-------------------------------------------------------------------------------
		// Upload Button
		//Upload all pending PODs to the server 
		m_btnUploadPendingPODs.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				if (m_cd.isConnectingToInternet() == false) 
				{					
					Toast.makeText(AdminActivity.this, "Connection Failed! Please check your internet connection", Toast.LENGTH_SHORT).show();
					return;
				}
				Intent intent = new Intent(AdminActivity.this, UploadActivity.class);
				startActivityForResult(intent, UPLOAD_ACTIVITY);
			}
		});
		

		//-------------------------------------------------------------------------------
		// Delete Button
		//Delete all pending PODs 
		m_btnDeletePendingPODs.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				//Show a confirmation dialog before deleting them all
				AlertDialog.Builder alert = new AlertDialog.Builder(AdminActivity.this);
				alert.setTitle(R.string.delete_confirmation_dialog_title);
				alert.setMessage(String.format(Locale.ENGLISH, "Are you sure you wish to remove all %d pending PODs ?", m_db.getPendingPODs().size()));
				alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {} 
				}); 
				alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						m_db.deletePendingPODs();
						UpdateUI();
					} }); 
				alert.show();
			}
		});

		
		//-------------------------------------------------------------------------------
		// Configuration Button
		m_btnConfiguration.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(AdminActivity.this, ConfigurationDlg.class);
				startActivity(i);
			}
		});


		//-------------------------------------------------------------------------------
		// Done
		Button done			= (Button) findViewById(R.id.action);
		done.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				finish();
			}
		});
	}


	///////////////////////////////////////////////////////////////////
    // Called at the start of the visible lifetime
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onResume() {
		super.onResume();
		UpdateUI();
	}

	///////////////////////////////////////////////////////////////////
    // Called at the end of the visible lifetime
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

	///////////////////////////////////////////////////////////////////
    // This is where we hear back from Activities that we have started
	///////////////////////////////////////////////////////////////////
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{
		switch(requestCode){
				
			case UPLOAD_ACTIVITY:
				UpdateUI();
				break;

			default:
				Log.e(TAG, "onActivityResult: Unknown RequestCode: " + requestCode);
				break;
		}
	}

	//Sync the UI to the current state
	private void UpdateUI() {
		int nPODs = m_db.getPendingPODs().size();
		m_btnUploadPendingPODs.setEnabled(nPODs > 0);
		m_btnDeletePendingPODs.setEnabled(nPODs > 0);
		
		switch(nPODs)
		{
		case 0:
			m_btnUploadPendingPODs.setText("No Collected PODs to Upload");
			break;
		case 1:
			m_btnUploadPendingPODs.setText("Upload 1 Collected POD");
			break;
		default:
			m_btnUploadPendingPODs.setText(String.format(Locale.ENGLISH, "Upload %d Collected PODs", nPODs));
			break;
		}
	}
}
