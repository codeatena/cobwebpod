package com.cobwebb.mobile.pod.collector;

import android.os.Parcel;
import android.os.Parcelable;

public class UploadResponse implements Parcelable {
	public int code;
	public String text;
	public String details;

	// Empty constructor
	public UploadResponse() {
		code = 0;
		text = "";
		details = "";
	}

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(code);
        out.writeString(text);
        out.writeString(details);
    }

    public static final Parcelable.Creator<UploadResponse> CREATOR
            = new Parcelable.Creator<UploadResponse>() {
        public UploadResponse createFromParcel(Parcel in) {
            return new UploadResponse(in);
        }

        public UploadResponse[] newArray(int size) {
            return new UploadResponse[size];
        }
    };
    
    private UploadResponse(Parcel in) {
    	code 	= in.readInt();
    	text 	= in.readString();
    	details = in.readString();
    }
}