package com.cobwebb.mobile.pod.collector;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.analytics.tracking.android.EasyTracker;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SignatureCapture extends Activity {

	public static final int SIGNEE_ACTIVITY = 100;
	LinearLayout mContent;
	signature mSignature;
	Button mClear, mNext, mCancel;
	public static String tempDir;
	public String mBarcodeData;
	public String mSigneeName;
	private Bitmap mBitmap;
	private Watermark mWatermark;
	View mView;
	File mypath;


	//private EditText yourName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle b  = getIntent().getExtras();
		if (b != null)
		{
			mBarcodeData = b.getString("barcode");
			mSigneeName  = b.getString("signeeName");
		}
		
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);		 
        //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.configuration_dialog_title);
		setContentView(R.layout.signature);
										               
		tempDir = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.external_dir) + "/";
		ContextWrapper cw = new ContextWrapper(getApplicationContext());
		File directory = cw.getDir(getResources().getString(R.string.external_dir), Context.MODE_PRIVATE);

		prepareDirectory();
		String uniqueId = createUniqueFilename();
		String filename = uniqueId + ".png";
		mypath = new File(directory, filename);

		mContent = (LinearLayout) findViewById(R.id.signatureViewHolder);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mWatermark = new Watermark();
		mSignature.setWatermark(mWatermark);
		mContent.addView(mSignature, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		String sWatermarkText = mBarcodeData + "\n" +  getCurrentTimestamp();
		mWatermark.setText(sWatermarkText);
		mClear = (Button) findViewById(R.id.clear);
		mNext = (Button) findViewById(R.id.next);
		mNext.setEnabled(false);
		mCancel = (Button) findViewById(R.id.cancel);
		mView = mContent;

		mClear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Panel Cleared");
				mSignature.clear();
				//mGetSign.setEnabled(false);
				mNext.setEnabled(false);
			}
		});

		/*
		mGetSign.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Panel Saved");
				boolean error = captureSignature();
				if (!error) {
					mView.setDrawingCacheEnabled(true);
					mSignature.save(mView);
					Bundle b = new Bundle();
					b.putString("status", "done");
					b.putString("imageUrl", mypath.getAbsolutePath());
					b.putString("fileName", mypath.getName());
					b.putString("driverName", yourName.getText().toString());
					Intent intent = new Intent();
					intent.putExtras(b);
					setResult(RESULT_OK, intent);
					finish();
				}
			}
		});
		*/
		
		mNext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Sign Saved.");
				
				mView.setDrawingCacheEnabled(true);
				mSignature.save(mView);
				Bundle b = new Bundle();
				b.putString("imageUrl", mypath.getAbsolutePath());
				b.putString("fileName", mypath.getName());
				b.putString("barcode", mBarcodeData);
				b.putString("signeeName", mSigneeName);
				
				Intent intent = new Intent();
				intent.putExtras(b);
				setResult(RESULT_OK, intent);
				finish();
				
			}
		});

		mCancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Panel Canceled");
				Intent intent = new Intent();
				setResult(RESULT_CANCELED, intent);
				finish();
			}
		});

	}

	
	@Override
	protected void onDestroy() {
		Log.w("GetSignature", "onDestory");
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{
		//if (requestCode == SIGNEE_ACTIVITY) 
		//{
			//Forward the result back up the Activity chain
			setResult(resultCode, intent);
			finish();
		//} 
	}
	
	private String getCurrentTimestamp() {
		String sRet;
		//SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.UK);
		//String sRet = s.format(new Date());//eg: 2013-09-23 15:54:37

		//test = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
		//sRet = test.format(new Date()); //23 Sep 2013 15:54:37
		
		DateFormat f = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
		sRet = f.format(new Date());
		return(sRet);
	}
	
	private String createUniqueFilename() {
		SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_hhmmssSSS", Locale.UK);
		String sRet = s.format(new Date()) + "_" + Math.random();
		return(sRet);
	}

	private boolean prepareDirectory() {
		try {
			if (makedirs()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(
					this,
					"Could not initiate File System.. Is Sdcard mounted properly?",
					Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	private boolean makedirs() {
		File tempdir = new File(tempDir);
		if (!tempdir.exists())
			tempdir.mkdirs();

		if (tempdir.isDirectory()) {
			File[] files = tempdir.listFiles();
			for (File file : files) {
				if (!file.delete()) {
					System.out.println("Failed to delete " + file);
				}
			}
		}
		return (tempdir.isDirectory());
	}

	public class signature extends View {
		private static final float STROKE_WIDTH = 5f;
		private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
		private Paint paint = new Paint();
		private Path path = new Path();

		private float lastTouchX;
		private float lastTouchY;
		private final RectF dirtyRect = new RectF();

		public signature(Context context, AttributeSet attrs) {
			super(context, attrs);
			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(STROKE_WIDTH);
			
		}

		public void save(View v) {
			Log.v("log_tag", "Width: " + v.getWidth());
			Log.v("log_tag", "Height: " + v.getHeight());
			if (mBitmap == null) {
				mBitmap = Bitmap.createBitmap(mContent.getWidth(),
						mContent.getHeight(), Bitmap.Config.RGB_565);
			}
			Canvas canvas = new Canvas(mBitmap);
			try {
				FileOutputStream mFileOutStream = new FileOutputStream(mypath);

				v.draw(canvas);
				mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
				mFileOutStream.flush();
				mFileOutStream.close();
				String url = Images.Media.insertImage(getContentResolver(),
						mBitmap, "title", null);
				Log.v("log_tag", "url: " + url);
				// In case you want to delete the file
				// boolean deleted = mypath.delete();
				// Log.v("log_tag","deleted: " + mypath.toString() + deleted);
				// If you want to convert the image to string use base64
				// converter

			} catch (Exception e) {
				Log.v("log_tag", e.toString());
			}
		}

		public void clear() {
			path.reset();
			invalidate();
		}

		@SuppressWarnings("deprecation")
		public void setWatermark(Watermark watermark) {
			setBackgroundDrawable(watermark);
		}
		
		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawPath(path, paint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float eventX = event.getX();
			float eventY = event.getY();
			//mGetSign.setEnabled(true);
			mNext.setEnabled(true);

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				path.moveTo(eventX, eventY);
				lastTouchX = eventX;
				lastTouchY = eventY;
				return true;

			case MotionEvent.ACTION_MOVE:

			case MotionEvent.ACTION_UP:

				resetDirtyRect(eventX, eventY);
				int historySize = event.getHistorySize();
				for (int i = 0; i < historySize; i++) {
					float historicalX = event.getHistoricalX(i);
					float historicalY = event.getHistoricalY(i);
					expandDirtyRect(historicalX, historicalY);
					path.lineTo(historicalX, historicalY);
				}
				path.lineTo(eventX, eventY);
				break;

			default:
				debug("Ignored touch event: " + event.toString());
				return false;
			}

			invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
					(int) (dirtyRect.top - HALF_STROKE_WIDTH),
					(int) (dirtyRect.right + HALF_STROKE_WIDTH),
					(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

			lastTouchX = eventX;
			lastTouchY = eventY;

			return true;
		}

		private void debug(String string) {
		}

		private void expandDirtyRect(float historicalX, float historicalY) {
			if (historicalX < dirtyRect.left) {
				dirtyRect.left = historicalX;
			} else if (historicalX > dirtyRect.right) {
				dirtyRect.right = historicalX;
			}

			if (historicalY < dirtyRect.top) {
				dirtyRect.top = historicalY;
			} else if (historicalY > dirtyRect.bottom) {
				dirtyRect.bottom = historicalY;
			}
		}

		private void resetDirtyRect(float eventX, float eventY) {
			dirtyRect.left = Math.min(lastTouchX, eventX);
			dirtyRect.right = Math.max(lastTouchX, eventX);
			dirtyRect.top = Math.min(lastTouchY, eventY);
			dirtyRect.bottom = Math.max(lastTouchY, eventY);
		}
	}
	
	public class Watermark extends Drawable {

	    private String text;
	    private TextPaint paint;

	    public Watermark() {
	    	this.paint = null;
	    }
	    
	    public TextPaint createPaint()
	    {
	    	TextPaint paint = new TextPaint();
	        paint.setColor(Color.rgb(250, 250, 250));
	        paint.setAntiAlias(true);
	        paint.setFakeBoldText(true);
	        paint.setShadowLayer(6f, 0, 0, Color.rgb(50, 50, 50));
	        paint.setStyle(Paint.Style.FILL);
	        paint.setTextAlign(Paint.Align.LEFT);
	        float textSize = calculateTextSize(paint, text, getBounds());
	        paint.setTextSize(textSize);
	        return(paint);
	    }

	    public void setText(String text){
	        this.text = text;
	        this.paint = null;
	    }
	    
	    @Override
	    public void draw(Canvas canvas) {
	    	if(paint == null)
	    		paint = createPaint();

	    	int oldColour = paint.getColor();
	        paint.setColor(Color.WHITE);
	    	canvas.drawPaint(paint); 
	        paint.setColor(oldColour);
	        
	    	StaticLayout layout = new StaticLayout(text, paint, canvas.getWidth(),Layout.Alignment.ALIGN_NORMAL, 1.3f, 0, false);
	    	layout.draw(canvas);
	    	//canvas.drawText(text, 0, canvas.getHeight(), paint);
	    }

	    @Override
	    public void setAlpha(int alpha) {
	        paint.setAlpha(alpha);
	    }

	    @Override
	    public void setColorFilter(ColorFilter cf) {
	        paint.setColorFilter(cf);
	    }

	    @Override
	    public int getOpacity() {
	        return PixelFormat.TRANSLUCENT;
	    }
	    
	    public float calculateTextSize(TextPaint paint, String text, Rect bounds){
	    	float textSize = 22f;//Minimum Text Size
	    	Rect textBounds = new Rect(0, 0, 0, 0);
	    	
	    	//Keep increasing the size of the font until it is too big to fit in bounds
	    	while((textBounds.width() <= bounds.width()) && (textBounds.height() <= bounds.height()))
	    	{
	    		textSize++;
	    		paint.setTextSize(textSize);
	 	    	//paint.getTextBounds(text, 0, text.length(), textBounds);
		    	StaticLayout layout = new StaticLayout(text, paint, bounds.width(),Layout.Alignment.ALIGN_NORMAL, 1.3f, 0, false);
		    	textBounds.right = layout.getWidth();
		    	textBounds.bottom = layout.getHeight();
	    	}
	    	
	    	textSize--;
	    	return(textSize);
	    }
	}
}
