package com.cobwebb.mobile.pod.collector;


import com.cobwebb.mobile.pod.collector.UploadActivity.LicenseCheckReceiver;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class LicenseCheckService extends IntentService {
	private static final String TAG = "LicenseCheckService";

	public LicenseCheckService() {
		super("LicenseCheckService");
	}
	
	public LicenseCheckService(String name){
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		String  sStatus = ""; 
		boolean bRet = false;
		String  sError = ""; 
		Intent result = new Intent(LicenseCheckReceiver.LICENSE_CHECKED);

		try{
	    	LicenseData license = LicenseData.runLicenseCheck(LicenseCheckService.this);
			sStatus = license.getLicenseStatus(); 
			bRet = license.isLicenseActive();
			sError = LicenseData.getLastError(); 
		} catch (Exception e) {
			sError = "Error running license check";
			Log.e(TAG, "Exception running runLicenseCheck", e);
		}
		result.putExtra(LicenseCheckReceiver.PARM_LICENSE_STATUS, sStatus);
		result.putExtra(LicenseCheckReceiver.PARM_LICENSE_CHECK_RESULT, bRet);
		result.putExtra(LicenseCheckReceiver.PARM_LICENSE_ERROR, sError);

		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.sendBroadcast(result);
	}
}
