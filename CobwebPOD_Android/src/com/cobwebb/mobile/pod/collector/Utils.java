package com.cobwebb.mobile.pod.collector;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.util.Log;

public class Utils {
	private static final String TAG = "Utils";
	
	public static String attachContent(String lineEnd, String twoHyphens, String boundary, String text, String name) 
	{
		String content = "";
		try
		{
			//I believe we should be using encodeURIComponent() or URLEncoder.encode(text, "UTF-8") here, but CHTTPD doesn't like either... 
			//	if you have a look at the Javadoc of URLEncoder, you see that it states: This class contains static methods for converting a String to the application/x-www-form-urlencoded MIME format.
			content += "Content-Disposition: form-data; name=\"" + name + "\"" + lineEnd;
			// content += "Content-Type: text/plain;charset=UTF-8" + lineEnd;
			// content += "Content-Length: " + text.length() + lineEnd;
			content += lineEnd;
			content += text + lineEnd;
			content += twoHyphens + boundary + lineEnd;
		} 
		catch (Exception e) 
		{
			Log.e(TAG, "ERROR: attachContent: " + e.getMessage(), e);
		}

		return content;
	}

	
	private final static char base64Array[] = { 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
			'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', '+', '/' };

	public static String base64Encode(String string) {
		String encodedString = "";
		byte bytes[] = string.getBytes();
		int i = 0;
		int pad = 0;
		while (i < bytes.length) {
			byte b1 = bytes[i++];
			byte b2;
			byte b3;
			if (i >= bytes.length) {
				b2 = 0;
				b3 = 0;
				pad = 2;
			} else {
				b2 = bytes[i++];
				if (i >= bytes.length) {
					b3 = 0;
					pad = 1;
				} else
					b3 = bytes[i++];
			}
			byte c1 = (byte) (b1 >> 2);
			byte c2 = (byte) (((b1 & 0x3) << 4) | (b2 >> 4));
			byte c3 = (byte) (((b2 & 0xf) << 2) | (b3 >> 6));
			byte c4 = (byte) (b3 & 0x3f);
			encodedString += base64Array[c1];
			encodedString += base64Array[c2];
			switch (pad) {
			case 0:
				encodedString += base64Array[c3];
				encodedString += base64Array[c4];
				break;
			case 1:
				encodedString += base64Array[c3];
				encodedString += "=";
				break;
			case 2:
				encodedString += "==";
				break;
			}
		}
		return encodedString;
	}

	public static String parseChttpdResponse(InputStream stream)
	{
		String sMsg = "";
		try
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(stream);
			Element p = document.getElementById("statusDescription");
			sMsg = getNodeText(p);
		}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			Log.e(TAG + " parseChttpdResponse", "Exception : " + e.getMessage(), e);
		}
		return(sMsg);
	}
	
	
	//extract all the child text nodes from a DOM node
	public static String getNodeText(Node node) 
	{
	    StringBuffer buf = new StringBuffer();
	    NodeList children = node.getChildNodes();
	    for (int i = 0; i < children.getLength(); i++) 
	    {
	        Node textChild = children.item(i);
	        if (textChild.getNodeType() == Node.TEXT_NODE) 
	        {
		        buf.append(textChild.getNodeValue());
	        }
	    }
	    return buf.toString();
	}
	
	public static String getVersionString(Context context){
		String sVersion = "";
		try {
			String s = context.getPackageManager().getPackageInfo(context.getPackageName(), 0 ).versionName;
			sVersion = "v" + s;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return(sVersion);
	}


	public static boolean isDebuggable(Context ctx) {
	    boolean debuggable = false;
	 
	    PackageManager pm = ctx.getPackageManager();
	    try
	    {
	        ApplicationInfo appinfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
	        debuggable = (0 != (appinfo.flags &= ApplicationInfo.FLAG_DEBUGGABLE));
	    }
	    catch(NameNotFoundException e)
	    {
	        /*debuggable variable will remain false*/
	    }
	     
	    return debuggable;
	}

	// a method for identifying when we are running on the emulator
	//The Build fingerprint is $(BRAND)/$(PRODUCT)/$(DEVICE)/$(BOARD):$(VERSION.RELEASE)/$(ID)/$(VERSION.INCREMENTAL):$(TYPE)/$(TAGS)
	//http://chall32.blogspot.co.uk/2010/06/android-market-fingerprints-demystified.html
	public static boolean isAndroidEmulator() {
	    String product = Build.PRODUCT;
	    Log.d(TAG, "product=" + product);
	    boolean isEmulator = false;
	    if (product != null) {
	        isEmulator = product.equals("sdk") || product.contains("_sdk") || product.contains("sdk_");
	    }
	    Log.d(TAG, "isEmulator=" + isEmulator);
	    return isEmulator;
	}	
	
	// * Utility class for JavaScript compatible UTF-8 encoding and decoding.
	//http://stackoverflow.com/questions/607176/java-equivalent-to-javascripts-encodeuricomponent-that-produces-identical-outpu
	public static String encodeURIComponent(String s)
	{
		String result = null;

		try
		{
			result = URLEncoder.encode(s, "UTF-8")
					.replaceAll("\\+", "%20")
					.replaceAll("\\%21", "!")
					.replaceAll("\\%27", "'")
					.replaceAll("\\%28", "(")
					.replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		}

		// This exception should never occur.
		catch (UnsupportedEncodingException e)
		{
			result = s;
		}

		return result;
	}  
	
	public static String decodeURIComponent(String s)
	{
		if (s == null)
		{
			return null;
		}

		String result = null;

		try
		{
			result = URLDecoder.decode(s, "UTF-8");
		}

		// This exception should never occur.
		catch (UnsupportedEncodingException e)
		{
			result = s;  
		}

		return result;
	}
}
