package com.cobwebb.mobile.pod.collector.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final String TAG = "DatabaseHandler";

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "com.cobwebb.mobile.pod.collector.db";

	// POD table name
	private static final String TABLE_POD = "pod";

	// POD Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_SIGNEE_NAME = "name";
	private static final String KEY_IMG_URL = "signature_img_url";
	private static final String KEY_BARCODE_DATA = "barcode_data";
	private static final String KEY_CAPTURE_DATE = "capture_date";
	private static final String KEY_IS_UPLOADED = "is_uploaded";

	// Config Table name
	private static final String TABLE_CONFIG = "config";

	// Config Table Columns names
	private static final String KEY_UPLOAD_URL = "upload_url";
	// private static final String KEY_DOCSTORE = "docstore";
	// private static final String KEY_DOCTYPE = "doctype";
	private static final String KEY_BARCODE_FIELD_NAME = "barcode";
	private static final String KEY_SIGNEE_FIELD_NAME = "signee_name";
	private static final String KEY_DRIVER_ID = "driver_id";
	private static final String KEY_COMPANY_NAME = "company_name";
	private static final String KEY_DRIVER_FIELD_NAME = "driver_name";
	private static final String KEY_UPLOAD_USER_NAME = "user_name";
	private static final String KEY_UPLOAD_PASSWORD = "password";

	// IVSTORE
	private static final String KEY_APPLICATION_AREA = "applicationArea";
	private static final String KEY_FOLIO_SUBJECT = "folioSubject";
	private static final String KEY_DOCUMENT_TYPE = "documentType";
	private static final String KEY_COMPONENT_TYPE = "componentType";
	private static final String KEY_FOLIO_TEXT = "folioText";
	private static final String KEY_DOCUMENT_TEXT_COMMENTS = "documentTextComments";
	private static final String KEY_USER_KEY = "userKey";
	private static final String KEY_IMAGE_VIEW_LIBRARY = "imageViewLibrary";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sCreateTablePOD = "CREATE TABLE " + TABLE_POD + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_SIGNEE_NAME + " TEXT,"
				+ KEY_BARCODE_DATA + " TEXT," 
				+ KEY_IMG_URL + " TEXT,"
				+ KEY_CAPTURE_DATE + " datetime default current_timestamp,"
				+ KEY_IS_UPLOADED + " INTEGER" + ")";

		db.execSQL(sCreateTablePOD);

		String sCreateTableConfig = "CREATE TABLE " + TABLE_CONFIG + "("
				+ KEY_UPLOAD_URL + " TEXT," 
				+ KEY_BARCODE_FIELD_NAME + " TEXT,"
				+ KEY_SIGNEE_FIELD_NAME + " TEXT," 
				+ KEY_DRIVER_ID + " TEXT,"
				+ KEY_COMPANY_NAME + " TEXT,"
				+ KEY_DRIVER_FIELD_NAME + " TEXT," 
				+ KEY_UPLOAD_USER_NAME + " TEXT,"
				+ KEY_UPLOAD_PASSWORD + " TEXT," 
				+ KEY_APPLICATION_AREA + " TEXT,"
				+ KEY_FOLIO_SUBJECT + " TEXT," 
				+ KEY_DOCUMENT_TYPE + " TEXT,"
				+ KEY_COMPONENT_TYPE + " TEXT," 
				+ KEY_FOLIO_TEXT + " TEXT,"
				+ KEY_DOCUMENT_TEXT_COMMENTS + " TEXT," 
				+ KEY_USER_KEY + " TEXT," + 
				KEY_IMAGE_VIEW_LIBRARY + " TEXT" + ")";

		db.execSQL(sCreateTableConfig);

		/*
		// Inserting Default Values into the table.
		ContentValues values = new ContentValues();
		values.put(KEY_UPLOAD_URL, 				"http://X.X.X.X:6400/rpc/cppdfileexitprogram/");
		values.put(KEY_UPLOAD_USER_NAME,		"");
		values.put(KEY_UPLOAD_PASSWORD, 		"");
		values.put(KEY_DRIVER_ID, 				"");
		values.put(KEY_COMPANY_NAME, 			"");

		values.put(KEY_BARCODE_FIELD_NAME, 		"");
		values.put(KEY_SIGNEE_FIELD_NAME, 		"");
		values.put(KEY_DRIVER_FIELD_NAME, 		"");

		// IVSTORE
		values.put(KEY_APPLICATION_AREA, 		"FINANCE");
		values.put(KEY_FOLIO_SUBJECT, 			"ORDERS");
		values.put(KEY_DOCUMENT_TYPE, 			"POD");
		values.put(KEY_COMPONENT_TYPE, 			"IMAGE");
		values.put(KEY_FOLIO_TEXT, 				"Proof of Delivery");
		values.put(KEY_DOCUMENT_TEXT_COMMENTS, 	"");
		values.put(KEY_USER_KEY, 				"");
		values.put(KEY_IMAGE_VIEW_LIBRARY, 		"PACIVOBJ");
		db.insert(TABLE_CONFIG, null, values);
		*/
		
		//TN Robinson
		ContentValues values = new ContentValues();
		values.put(KEY_UPLOAD_URL, 				"http://10.1.1.2:6400/rpc/cppdfileexitprogram/");
		values.put(KEY_UPLOAD_USER_NAME,		"");
		values.put(KEY_UPLOAD_PASSWORD, 		"");
		values.put(KEY_DRIVER_ID, 				"");
		values.put(KEY_COMPANY_NAME, 			"TN Robinson");

		values.put(KEY_BARCODE_FIELD_NAME, 		"");
		values.put(KEY_SIGNEE_FIELD_NAME, 		"");
		values.put(KEY_DRIVER_FIELD_NAME, 		"");

		// IVSTORE
		values.put(KEY_APPLICATION_AREA, 		"FINANCE");
		values.put(KEY_FOLIO_SUBJECT, 			"ORDERS");
		values.put(KEY_DOCUMENT_TYPE, 			"POD");
		values.put(KEY_COMPONENT_TYPE, 			"IMAGE");
		values.put(KEY_FOLIO_TEXT, 				"Proof of Delivery");
		values.put(KEY_DOCUMENT_TEXT_COMMENTS, 	"");
		values.put(KEY_USER_KEY, 				"");
		values.put(KEY_IMAGE_VIEW_LIBRARY, 		"PACIVOBJ");
		db.insert(TABLE_CONFIG, null, values);
	}

	// Upgrading database
	// Note: It seems SQLite3 only allows to add one column at a time,
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (newVersion > oldVersion) {
			// Upgrade
			switch(oldVersion) {
				case 1:
					// Upgrade from version 1 to 2
					try {
						//Version 2 modifications
						db.execSQL("ALTER TABLE " + TABLE_CONFIG + " ADD COLUMN " + KEY_COMPANY_NAME + " TEXT;");
					} catch (SQLException e) {
						Log.e(TAG, "Error executing SQL: ", e);
						// If the error is "duplicate column name" then everything is fine,
						// as this happens after upgrading 2->3, then downgrading 3->2,
						// and then upgrading again 2->3.
					}
					// fall through for further upgrades.

				//case 2:
					// Upgrade from version 2 to 3
					//break after all upgrades have been done
					break;
	
				default:
					Log.w(TAG, "Unknown version " + oldVersion + ". Creating new database.");
					// Drop older table if existed
					db.execSQL("DROP TABLE IF EXISTS " + TABLE_POD);
					db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIG);
			
					// Create tables again
					onCreate(db);
					break;
			}
		} 
		else 
		{ // newVersion <= oldVersion
			// Downgrade
			Log.w(TAG, "Don't know how to downgrade. Will not touch database and hope they are compatible.");
			// Do nothing.
		}
	}

	private Date getDateFromString(String sDate) {
		Date date = null;
		SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		try {
			date = date_format.parse(sDate);
		} catch (ParseException e) {
			Log.e(TAG, "Parsing ISO8601 datetime failed", e);
		}
		return date;
	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new POD
	public void addPOD(POD pod) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SIGNEE_NAME, pod.getSigneeName());
		values.put(KEY_BARCODE_DATA, pod.getBarcodeData());
		values.put(KEY_IMG_URL, pod.getSignatureImageUrl());
		values.put(KEY_IS_UPLOADED, pod.getIsUploaded());
		// Inserting Row
		db.insert(TABLE_POD, null, values);
		db.close(); // Closing database connection
	}

	// Getting single POD
	public POD getPOD(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_POD, new String[] { KEY_ID, KEY_SIGNEE_NAME, KEY_BARCODE_DATA, KEY_IMG_URL, KEY_CAPTURE_DATE, KEY_IS_UPLOADED }
									, KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		POD pod = createPOD(cursor);
		return pod;
	}

	private POD createPOD(Cursor cursor) {
		POD pod = new POD();

		pod.setID(Integer.parseInt(cursor.getString(0)));
		pod.setSigneeName(cursor.getString(1));
		pod.setBarcodeData(cursor.getString(2));
		pod.setSignatureImageUrl(cursor.getString(3));
		Date date = getDateFromString(cursor.getString(4));
		pod.setCaptureDate(date);
		pod.setIsUploaded(Integer.parseInt(cursor.getString(5)));
		
		return(pod);
	}
	// Getting All pending PODs
	public List<POD> getPendingPODs() 
	{
		List<POD> podList = new ArrayList<POD>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POD + " WHERE " + KEY_IS_UPLOADED + " = 0";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) 
		{
			do 
			{
				POD pod = createPOD(cursor);
				// Adding POD to list
				podList.add(pod);
			}while (cursor.moveToNext());
		}

		// return list
		return podList;
	}
	
	// Updating single POD
	public int updatePOD(POD pod) 
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_SIGNEE_NAME, pod.getSigneeName());
		values.put(KEY_BARCODE_DATA, pod.getBarcodeData());
		values.put(KEY_IMG_URL, pod.getSignatureImageUrl());
		values.put(KEY_IS_UPLOADED, pod.getIsUploaded());

		// updating row
		return db.update(TABLE_POD, values, KEY_ID + " = ?", new String[] { String.valueOf(pod.getID()) });
	}

	// Deleting single POD
	public void deletePOD(POD pod) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_POD, KEY_ID + " = ?",
				new String[] { String.valueOf(pod.getID()) });
		db.close();
	}

	// Deleting all pending PODs
	public void deletePendingPODs() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_POD, KEY_IS_UPLOADED + " = ?",new String[] {"0"});
		db.close();
	}

	// How many PODs have we collected but not uploaded ?
	public int getPendingPODCount() {
		String countQuery = "SELECT  * FROM " + TABLE_POD + " WHERE "
				+ KEY_IS_UPLOADED + " = 0";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	// How many PODs have we uploaded ever?
	public int getUploadedPODCount() {
		String countQuery = "SELECT  * FROM " + TABLE_POD + " WHERE "
				+ KEY_IS_UPLOADED + " = 1";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	public void saveField(Configuration field) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_UPLOAD_URL, field.getUploadURL());
		values.put(KEY_UPLOAD_USER_NAME, field.getUserName());
		values.put(KEY_UPLOAD_PASSWORD, field.getPassword());
		values.put(KEY_DRIVER_ID, field.getDriverId());
		values.put(KEY_COMPANY_NAME, field.getCompanyName());

		values.put(KEY_BARCODE_FIELD_NAME, field.getBarcodeFieldname());
		values.put(KEY_SIGNEE_FIELD_NAME, field.getSigneeFieldname());
		values.put(KEY_DRIVER_FIELD_NAME, field.getDriverFieldname());

		// IVSTORE
		values.put(KEY_APPLICATION_AREA, field.getApplicationArea());
		values.put(KEY_FOLIO_SUBJECT, field.getFolioSubject());
		values.put(KEY_DOCUMENT_TYPE, field.getDocumentType());
		values.put(KEY_COMPONENT_TYPE, field.getComponentType());
		values.put(KEY_FOLIO_TEXT, field.getFolioText());
		values.put(KEY_DOCUMENT_TEXT_COMMENTS, field.getDocumentTextComments());
		values.put(KEY_USER_KEY, field.getUserKey());
		values.put(KEY_IMAGE_VIEW_LIBRARY, field.getImageViewLibrary());

		db.update(TABLE_CONFIG, values, null, null);
		db.close(); // Closing database connection
	}

	// Getting single Field
	public Configuration getConfiguration() {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CONFIG, new String[] { KEY_UPLOAD_URL,
				KEY_UPLOAD_USER_NAME, 
				KEY_UPLOAD_PASSWORD, 
				KEY_DRIVER_ID, 
				KEY_COMPANY_NAME, 
				KEY_BARCODE_FIELD_NAME, 
				KEY_SIGNEE_FIELD_NAME, 
				KEY_DRIVER_FIELD_NAME,
				KEY_APPLICATION_AREA,
				KEY_FOLIO_SUBJECT, 
				KEY_DOCUMENT_TYPE, 
				KEY_COMPONENT_TYPE,
				KEY_FOLIO_TEXT, 
				KEY_DOCUMENT_TEXT_COMMENTS, 
				KEY_USER_KEY,
				KEY_IMAGE_VIEW_LIBRARY }, null, null, null, null, null);

		Configuration config = null;

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			config = new Configuration();
			config.setUploadURL(cursor.getString(0));
			config.setUserName(cursor.getString(1));
			config.setPassword(cursor.getString(2));
			config.setDriverId(cursor.getString(3));
			config.setCompanyName(cursor.getString(4));

			config.setBarcodeFieldname(cursor.getString(5));
			config.setSigneeFieldname(cursor.getString(6));
			config.setDriverFieldname(cursor.getString(7));

			config.setApplicationArea(cursor.getString(8));
			config.setFolioSubject(cursor.getString(9));
			config.setDocumentType(cursor.getString(10));
			config.setComponentType(cursor.getString(11));
			config.setFolioText(cursor.getString(12));
			config.setDocumentTextComments(cursor.getString(13));
			config.setUserKey(cursor.getString(14));
			config.setImageViewLibrary(cursor.getString(15));
		}

		return(config);
	}

}
