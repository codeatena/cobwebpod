package com.cobwebb.mobile.pod.collector.database;

import java.util.Date;

public class POD {

	public static final String DATA_SEPARATOR = ",";

	// private variables
	int 	_id = -1;
	String 	_signeeName;
	String 	_barcode_data;
	String 	_signature_img_url;
	Date 	_capture_date;
	int 	_is_uploaded = 0;

	// Empty constructor
	public POD() {
	}

	public int getID() {
		return this._id;
	}
	public void setID(int id) {
		this._id = id;
	}

	public String getSigneeName() {
		return this._signeeName;
	}
	public void setSigneeName(String name) {
		this._signeeName = name;
	}

	public String getBarcodeData() {
		return this._barcode_data;
	}
	public void setBarcodeData(String barcode_data) {
		this._barcode_data = barcode_data;
	}

	public String getSignatureImageUrl() {
		return this._signature_img_url;
	}
	public void setSignatureImageUrl(String signature_img_url) {
		this._signature_img_url = signature_img_url;
	}

	public Date getCaptureDate() {
		return this._capture_date;
	}
	public void setCaptureDate(Date capture_date) {
		this._capture_date = capture_date;
	}

	public int getIsUploaded() {
		return this._is_uploaded;
	}
	public void setIsUploaded(int is_uploaded) {
		this._is_uploaded = is_uploaded;
	}
}
