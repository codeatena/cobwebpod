package com.cobwebb.mobile.pod.collector.database;

public class Configuration {
	// private variables
		String _uplodURL;
		String _userName;
		String _password;
		String _driverId;
		String _companyName;

		// DOCSTORE SPECIFIC
		//this._docstore = docstore;
		//this._doctype = doctype;
		String _barcodeFieldname;
		String _signeeFieldname;
		String _driverFieldname;
		
		// IVSTORE
		String _ApplicationArea;
		String _FolioSubject;
		String _DocumentType;
		String _ComponentType;
		String _FolioText;
		String _DocumentTextComments;	//Not used - We store the Signee Name in the document text comments
		String _UserKey;				//Not used - We store the Barcode data in the user key
		String _ImageViewLibrary;
		
		// Empty constructor
		public Configuration() {

		}

		public boolean isValid(){
			return(true);
		}
		// constructor
/*		public Configuration(String uploadURL, 
				String barcodeFieldname,
				String signeeFieldname, 
				String driverId, 
				String companyName, 
				String driverFieldname, 
				String userName, 
				String password,
				String applicationArea,
				String folioSubject,
				String documentType,
				String componentType,
				String folioText,
				String documentTextComments,
				String userKey,
				String imageViewLibrary ) 
		{
			this._uplodURL = uploadURL;
			this._userName = userName;
			this._password = password;
			this._driverId = driverId;			
			this._companyName = companyName;			
			
			// DOCSTORE SPECIFIC
			//this._docstore = docstore;
			//this._doctype = doctype;
			this._barcodeFieldname = barcodeFieldname;
			this._signeeFieldname = signeeFieldname;
			this._driverFieldname = driverFieldname;

			// IVSTORE SPECIFIC
			this._ApplicationArea = applicationArea;
			this._FolioSubject = folioSubject;
			this._DocumentType = documentType;
			this._ComponentType = componentType;
			this._FolioText = folioText;
			this._DocumentTextComments = documentTextComments;
			this._UserKey = userKey;
			this._ImageViewLibrary = imageViewLibrary;
		}
*/				
		public String getUploadURL() {
			return this._uplodURL;
		}
		public void setUploadURL(String uploadURL) {
			this._uplodURL = uploadURL;
		}		
		
		public String getUserName() {
			return this._userName;
		}
		public void setUserName(String userName) {
			this._userName = userName;
		}
		
		public String getPassword() {
			return this._password;
		}
		public void setPassword(String password) {
			this._password = password;
		}
		
		public String getDriverId() {
			return this._driverId;
		}
		public void setDriverId(String driverId) {
			this._driverId = driverId;
		}	

		public String getCompanyName() {
			return this._companyName;
		}
		public void setCompanyName(String companyName) {
			this._companyName = companyName;
		}	

		////////////////////////////////////////////
		//DOCSTORE SPECIFIC
		
/*		// getting docstore
		public String getDocstore() {
			return this._docstore;
		}
		public void setDocstore(String docstore) {
			this._docstore = docstore;
		}
		
		public String getDoctype() {
			return this._doctype;
		}
		public void setDoctype(String doctype) {
			this._doctype = doctype;
		}
*/

		public String getBarcodeFieldname() {
			return this._barcodeFieldname;
		}
		public void setBarcodeFieldname(String barcode) {
			this._barcodeFieldname = barcode;
		}

		public String getSigneeFieldname() {
			return this._signeeFieldname;
		}
		public void setSigneeFieldname(String signeeFieldname) {
			this._signeeFieldname = signeeFieldname;
		}

		public String getDriverFieldname() {
			return this._driverFieldname;
		}
		public void setDriverFieldname(String driverName) {
			this._driverFieldname = driverName;
		}	
		
		
		////////////////////////////////////////////
		// IVSTORE SPECIFIC
		
		public String getApplicationArea() {
			return this._ApplicationArea;
		}
		public void setApplicationArea(String applicationArea) {
			this._ApplicationArea = applicationArea;
		}
		
		public String getFolioSubject() {
			return this._FolioSubject;
		}
		public void setFolioSubject(String folioSubject) {
			this._FolioSubject = folioSubject;
		}
		
		public String getDocumentType() {
			return this._DocumentType;
		}
		public void setDocumentType(String documentType) {
			this._DocumentType = documentType;
		}
		
		public String getComponentType() {
			return this._ComponentType;
		}
		public void setComponentType(String componentType) {
			this._ComponentType = componentType;
		}
		
		public String getFolioText() {
			return this._FolioText;
		}
		public void setFolioText(String folioText) {
			this._FolioText = folioText;
		}
		
		public String getDocumentTextComments() {
			return this._DocumentTextComments;
		}
		public void setDocumentTextComments(String documentTextComments) {
			this._DocumentTextComments = documentTextComments;
		}
		
		public String getUserKey() {
			return this._UserKey;
		}
		public void setUserKey(String userKey) {
			this._UserKey = userKey;
		}
		
		public String getImageViewLibrary() {
			return this._ImageViewLibrary;
		}
		public void setImageViewLibrary(String imageViewLibrary) {
			this._ImageViewLibrary = imageViewLibrary;
		}
}
