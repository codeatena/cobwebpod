package com.cobwebb.mobile.pod.collector;

import java.util.Locale;

import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;
import com.cobwebb.mobile.pod.collector.database.POD;
import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
/*
 * This is the main class for our App. It is an 'Activity' which is like a discreet unit of work.
 * We have other activities for getting the barcode, the signature and the signees name. Also for the config dialog
 */
public class PODCollector extends Activity {
	private static final String TAG = "PODCollector";

	
	public static final int MANUAL_BARCODE_ENTRY_ACTIVITY = 60;
	public static final int SIGNEE_ACTIVITY = 70;
	
	Button 		m_btnScan;
	Button 		m_btnManualBarcode;
	Button 		m_btnSign;
	Button		m_btnSignee;
	Button 		m_btnClear;
	TextView 	m_barcodeData;
	TextView	m_signeeData;
	TextView 	m_signatureFilePath;
	ImageView 	m_signatureFileImage;
	TextView 	m_signeeName;
	
	POD			m_activePOD = null;
	
	ConnectionDetector 	m_cd;
	DatabaseHandler 	m_db;
	Bitmap 				m_logo;
	///////////////////////////////////////////////////////////////////
	// Called when the activity is first created. 
    ///////////////////////////////////////////////////////////////////
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        setContentView(R.layout.main); 
		
		m_db = new DatabaseHandler(this);
		m_cd = new ConnectionDetector(getApplicationContext());
		
		//m_btnAdministration 	= (Button)findViewById(R.id.administration);
		m_btnScan 				= (Button) findViewById(R.id.scan);
		m_btnManualBarcode 		= (Button) findViewById(R.id.manualBarcodeBtn);
		m_btnSign 				= (Button) findViewById(R.id.signature);
		m_btnSignee				= (Button) findViewById(R.id.signee);
		m_btnClear 				= (Button) findViewById(R.id.clear);
		m_barcodeData 			= (TextView) findViewById(R.id.barcodeData);
		m_signeeName 			= (TextView) findViewById(R.id.signeeName);
		m_signatureFileImage 	= (ImageView) findViewById(R.id.signatureImage);		
		m_signatureFilePath 	= (TextView) findViewById(R.id.imgURL);
		
		//-------------------------------------------------------------------------------
		// Admininstration Button
		/*m_btnAdministration.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(PODCollector.this, PasswordDlg.class);
				i.putExtra(PasswordDlg.PARM_SUCCESS_ACTIVITY,  AdminActivity.class.getName());
				startActivity(i);
				reset();
			}
		});*/

		
		//-------------------------------------------------------------------------------
		// Scan Button
		m_btnScan.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				if(m_signatureFilePath.getText().length() > 0)
					reset();
				
				//DWMOD - for testing in the emulator (ie when you don't have a camera attached. 
				if(Utils.isAndroidEmulator() == true)
				{
					emulateBarcodeScan();
					return;
				}
				
				//call the barcode scanner app
				BarcodeScanner scanner = new BarcodeScanner(PODCollector.this);
				// scanner.addExtra("SCAN_MODE", "ONE_D_MODE");
				// scanner.addExtra("SCAN_FORMATS", "CODE_39");
				scanner.initiateScan();
				
			}
		});

		//-------------------------------------------------------------------------------
		// Scan Button
		m_btnManualBarcode.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				if(m_signatureFilePath.getText().length() > 0)
					reset();
				
				Bundle b = new Bundle();
				b.putString("barcodeData", m_barcodeData.getText().toString());
				Intent intent = new Intent(PODCollector.this, ManualBarcodeCapture.class);
				intent.putExtras(b);
				startActivityForResult(intent, MANUAL_BARCODE_ENTRY_ACTIVITY);
			}
		});

		//-------------------------------------------------------------------------------
		// Sign Button
		m_btnSign.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Bundle b = new Bundle();
				b.putString("barcode", m_barcodeData.getText().toString());
				Intent intent = new Intent(PODCollector.this, SignatureCapture.class);
				intent.putExtras(b);
			//	startActivityForResult(intent, SIGNATURE_ACTIVITY);
			}
		});

		
		//-------------------------------------------------------------------------------
		// Signee Button
				m_btnSignee.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						Bundle b = new Bundle();
						b.putString("barcode", m_barcodeData.getText().toString());
						Intent intent = new Intent(PODCollector.this, SigneeCapture.class);
						intent.putExtras(b);
						startActivityForResult(intent, SIGNEE_ACTIVITY);
					}
				});
		
				
				
				
				
		//-------------------------------------------------------------------------------
		// Clear Button
		//Clears any current Confirmation data  
		m_btnClear.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				reset();
				/*
				 //Show a confirmation dialog before clearing the current POD data
				AlertDialog.Builder alert = new AlertDialog.Builder(PODCollector.this);
				alert.setTitle(R.string.clear_current_data_confirmation_title);
				alert.setMessage(R.string.clear_current_data_confirmation_text);
				alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {} 
				}); 
				alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						reset();
					} 
				}); 
				//alert.setIcon(R.drawable.icon);
				alert.show();
				 */
			}
		});


		String sig = String.format(Locale.ENGLISH, getString(R.string.app_signature), Utils.getVersionString(getApplicationContext()));
		TextView appSig = (TextView) findViewById(R.id.appSignature);
		appSig.setText(sig);
		
		
		m_logo = BitmapFactory.decodeResource(getResources(), R.drawable.logo);

		reset();

		if(savedInstanceState != null)
		{
			if(savedInstanceState.getString("barcodeData") != null && !savedInstanceState.getString("barcodeData").equalsIgnoreCase(""))
				m_barcodeData.setText(savedInstanceState.getString("barcodeData"));

			if(savedInstanceState.getString("signatureFilePath") != null && !(savedInstanceState.getString("signatureFilePath").equalsIgnoreCase("")))
			{
				Bitmap bitmap = BitmapFactory.decodeFile(savedInstanceState.getString("signatureFilePath"));
				m_signatureFileImage.setImageBitmap(bitmap);
				m_signatureFilePath.setText(savedInstanceState.getString("signatureFilePath"));
			}
			
			if(savedInstanceState.getString("signeeName") != null && !savedInstanceState.getString("signeeName").equalsIgnoreCase(""))
				m_signeeName.setText(savedInstanceState.getString("signeeName"));

		}
	}

	
	
	///////////////////////////////////////////////////////////////////
    // Called at the start of the visible lifetime
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onStart() {
		super.onStart();
		updateUI();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}

	///////////////////////////////////////////////////////////////////
    // Called after onStart, use to restore UI state.
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	
	///////////////////////////////////////////////////////////////////
    // Called at the start of the active lifetime
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onResume() {
		super.onResume();
		updateUI();
	}

	///////////////////////////////////////////////////////////////////
    // Called at the end of the visible lifetime
	///////////////////////////////////////////////////////////////////
	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

	///////////////////////////////////////////////////////////////////
    // Called to save UI state changes at the end of the active lifecycle
	///////////////////////////////////////////////////////////////////
    @Override
	protected void onSaveInstanceState(Bundle savedInstanceState) 
	{		 		
		super.onSaveInstanceState(savedInstanceState);

		if(m_barcodeData.getText().length() > 0)		
			savedInstanceState.putString("barcodeData", m_barcodeData.getText().toString());
	       
		if(m_signatureFilePath.getText().length() > 0)		
			savedInstanceState.putString("signatureFilePath", m_signatureFilePath.getText().toString());

		if(m_signeeName.getText().length() > 0)		
			savedInstanceState.putString("signeeName", m_signeeName.getText().toString());
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu); 
	}
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		switch(item.getItemId())
		{
			case R.id.menu_showAdminActivity:
				Intent i = new Intent(PODCollector.this, PasswordDlg.class);
				i.putExtra(PasswordDlg.PARM_SUCCESS_ACTIVITY,  AdminActivity.class.getName());
				startActivity(i);
				reset();
				return(true);
			default:
				return false;
		}
	}



	///////////////////////////////////////////////////////////////////
    // This is where we hear back from Activities that we have started
	///////////////////////////////////////////////////////////////////
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{
		switch(requestCode){

			case MANUAL_BARCODE_ENTRY_ACTIVITY: 
				if (resultCode == RESULT_OK) 
				{
					Bundle bundle = intent.getExtras();
	
					String barcodeData = bundle.getString("barcodeData"); 
					addBarcodeData(barcodeData);
	
					Toast.makeText(this, "Manual Barcode Entry collected", Toast.LENGTH_SHORT).show();
				}
				else if(resultCode == RESULT_CANCELED)
					Toast.makeText(this, "Manual Barcode Entry Cancelled", Toast.LENGTH_LONG).show();
	
				break;
				
			case SIGNEE_ACTIVITY: 
				if (resultCode == RESULT_OK) 
				{
					
					Bundle bundle = intent.getExtras();
	
					String signeeName = bundle.getString("signeeName");
					String barcode = bundle.getString("barcode"); 
					String imageUrl = bundle.getString("imageUrl");
					
					POD pod = new POD();
					pod.setSigneeName(signeeName);
					pod.setBarcodeData(barcode);
					pod.setSignatureImageUrl(imageUrl);
					m_db.addPOD(pod);
	
					m_barcodeData.setText(barcode);
					m_signeeName.setText(signeeName);
					Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
					m_signatureFileImage.setImageBitmap(bitmap);
					m_signatureFilePath.setText(imageUrl);
					updateUI();		
	
					Toast.makeText(this, "POD collected", Toast.LENGTH_SHORT).show();
				}
				else 
					Toast.makeText(this, "Signature capture failed", Toast.LENGTH_LONG).show();
	
				break;
			
			
			case BarcodeScanner.REQUEST_CODE:
				BarcodeScannerResult scanResult = BarcodeScanner.parseActivityResult(requestCode, resultCode, intent);
				if (scanResult != null) 
				{
					if (scanResult.getFormatName() != null && scanResult.getContents() != null) 
					{
						addBarcodeData(scanResult.getContents());
					}
				} 
				else 
				{
					Toast.makeText(PODCollector.this, "Invalid Barcode format. Please scan other Barcode", Toast.LENGTH_LONG).show();
				}
				break;
			
			
			default:
				Log.e(TAG, "onActivityResult: Unknown RequestCode: " + requestCode);
				break;
		}
	}

	
	//Don't allow duplicates
	private void addBarcodeData(String sNewData) {
		sNewData = sNewData.trim();
		
		String sCurData = m_barcodeData.getText().toString();
		String[] data = sCurData.split("\\" + POD.DATA_SEPARATOR);
		
		for(int nIndex=0; nIndex < data.length; nIndex++)
		{
			if(sNewData.compareToIgnoreCase(data[nIndex]) == 0)
			{
				Toast.makeText(PODCollector.this, "Duplicate Data Ignored: " + sNewData, Toast.LENGTH_LONG).show();
				return;
			}
		}
		if(sCurData.length() > 0)
			sCurData += POD.DATA_SEPARATOR + " ";
		sCurData += sNewData;
		m_barcodeData.setText(sCurData);
		updateUI();		
	}



	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////
    // Helpers

	public void emulateBarcodeScan() {
		String bdta = "12345";
		m_barcodeData.setText(bdta);
		updateUI();		
	}
	
	//Reset the UI to the default state
	private void reset() {	

		m_signatureFileImage.setImageResource(android.R.color.transparent);
		try{
			m_signatureFileImage.setImageBitmap(m_logo);
		} catch (Exception e) {
			Log.w(TAG, "Failed to load icon", e);
		}

		m_barcodeData.setText("");
		m_signeeName.setText("");
		m_signatureFilePath.setText("");
		
		updateUI();
	}

	//Sync the UI to the current state
	private void updateUI() {
		CharSequence data = m_barcodeData.getText();
		boolean bHaveSomeConfirmationData = data.length() > 0;

		boolean bHaveAllConfirmationData = false;
		if(bHaveSomeConfirmationData == true)
		{
			CharSequence signee = m_signeeName.getText();
			if(signee.length() > 0)
			{
				bHaveAllConfirmationData = true;
				bHaveSomeConfirmationData = false;
			}
		}
		
		int nVisibilityConfirmation = View.GONE;
		int nVisibilityBranding = View.GONE;
		if((bHaveSomeConfirmationData == true) || (bHaveAllConfirmationData == true))
			nVisibilityConfirmation = View.VISIBLE;
		else
			nVisibilityBranding = View.VISIBLE; 
		
		findViewById(R.id.appSignatureRow).setVisibility(nVisibilityBranding);
		findViewById(R.id.appSignatureRow2).setVisibility(nVisibilityBranding);
		findViewById(R.id.barcodeDataRow).setVisibility(nVisibilityConfirmation);
		findViewById(R.id.signeeNameRow).setVisibility(nVisibilityConfirmation);
		
		findViewById(R.id.signatureImageRow).setVisibility(View.VISIBLE);
		findViewById(R.id.imgURLRow).setVisibility(View.GONE);
		findViewById(R.id.barcodeRow).setVisibility(View.GONE);

		m_btnSign.setEnabled(bHaveSomeConfirmationData);
		m_btnClear.setEnabled((bHaveSomeConfirmationData == true) || (bHaveAllConfirmationData == true));
		updateSig2();
	}




	private void updateSig2() {
		int nCollected 	= m_db.getPendingPODCount();
		int nUploaded 	= m_db.getUploadedPODCount();
		Resources res = getResources();
		String sig2 =  "";
		if(nCollected > 0)
			sig2 = res.getQuantityString(R.plurals.pod_collected, nCollected, nCollected);
		if(nUploaded > 0)
		{
			if(nCollected > 0)
				sig2 += " / ";
			sig2 += res.getQuantityString(R.plurals.pod_uploaded, nUploaded, nUploaded);
		}

		TextView appSig2 = (TextView) findViewById(R.id.appSignature2);
		appSig2.setText(sig2);
	}
}