package com.cobwebb.mobile.pod.collector;

import com.cobwebb.mobile.pod.collector.database.Configuration;
import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;
import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ConfigurationDlg extends Activity{
		
	EditText uploadURL;
	EditText barcodeFieldname;
	EditText signeeFieldname;
	EditText driverId;
	EditText companyName;
	EditText driverFieldname;
	EditText userName;
	EditText password;
	
	// IVSTORE
	EditText applicationArea;
	EditText folioSubject;
	EditText documentType;
	EditText componentType;
	EditText folioText;
	EditText documentTextComments;
	EditText userKey;
	EditText imageViewLibrary;
	
	Button submit, cancel;
	DatabaseHandler db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.configuration_dialog); 

		String newTitle = getTitle().toString() + " " + Utils.getVersionString(getApplicationContext());
		setTitle(newTitle);

        db = new DatabaseHandler(this);
				
		uploadURL 			= (EditText)findViewById(R.id.uploadURL);
		userName  			= (EditText)findViewById(R.id.userName);
		password  			= (EditText)findViewById(R.id.password);
		driverId  			= (EditText)findViewById(R.id.driverId);
		companyName  		= (EditText)findViewById(R.id.companyName);

		barcodeFieldname 	= (EditText)findViewById(R.id.barcodeFieldname);
		signeeFieldname 	= (EditText)findViewById(R.id.signeeFieldname);
		driverFieldname  	= (EditText)findViewById(R.id.driverFieldname2);
		
		// IVSTORE
		applicationArea 	= (EditText)findViewById(R.id.applicationArea);
		folioSubject 		= (EditText)findViewById(R.id.folioSubject);
		documentType 		= (EditText)findViewById(R.id.documentType);
		componentType 		= (EditText)findViewById(R.id.componentType);
		folioText 			= (EditText)findViewById(R.id.folioText);
		documentTextComments = (EditText)findViewById(R.id.documentTextComments);
		userKey 			= (EditText)findViewById(R.id.userKey);
		imageViewLibrary 	= (EditText)findViewById(R.id.imageViewLibrary);
		
		submit = (Button)findViewById(R.id.submit);
		cancel = (Button)findViewById(R.id.cancel);
		
		Configuration field = db.getConfiguration();
		if(field != null)
		{
			uploadURL.setText(field.getUploadURL());
			userName.setText(field.getUserName());
			password.setText(field.getPassword());		
			driverId.setText(field.getDriverId());
			companyName.setText(field.getCompanyName());
			
			barcodeFieldname.setText(field.getBarcodeFieldname());
			signeeFieldname.setText(field.getSigneeFieldname());
			driverFieldname.setText(field.getDriverFieldname());
			
			// IVSTORE
			applicationArea.setText(field.getApplicationArea());
			folioSubject.setText(field.getFolioSubject());
			documentType.setText(field.getDocumentType());
			componentType.setText(field.getComponentType());
			folioText.setText(field.getFolioText());
			documentTextComments.setText(field.getDocumentTextComments());
			userKey.setText(field.getUserKey());
			imageViewLibrary.setText(field.getImageViewLibrary());
		}
		
		submit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
								
				Configuration config = new Configuration();
				config.setUploadURL(uploadURL.getText().toString().trim());
				config.setUserName(userName.getText().toString().trim());
				config.setPassword(password.getText().toString().trim());
				config.setDriverId(driverId.getText().toString().trim());
				config.setCompanyName(companyName.getText().toString().trim());

				config.setBarcodeFieldname(barcodeFieldname.getText().toString().trim());
				config.setSigneeFieldname(signeeFieldname.getText().toString().trim());
				config.setDriverFieldname(driverFieldname.getText().toString().trim());

				config.setApplicationArea(applicationArea.getText().toString().trim());
				config.setFolioSubject(folioSubject.getText().toString().trim());
				config.setDocumentType(documentType.getText().toString().trim());
				config.setComponentType(componentType.getText().toString().trim());
				config.setFolioText(folioText.getText().toString().trim());
				config.setDocumentTextComments(documentTextComments.getText().toString().trim());
				config.setUserKey(userKey.getText().toString().trim());
				config.setImageViewLibrary(imageViewLibrary.getText().toString().trim());
				
				if(config.isValid() == false )
				{
					showOKDialog("Error", "Please fill in the configuration values in all the fields.");
				}
				else
				{
					db.saveField(config);
					Toast.makeText(ConfigurationDlg.this, "Configuration Updated", Toast.LENGTH_LONG).show();
					finish();
				}											
			}
		});			
	
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});			
	
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

	public void showOKDialog(final String sTitle, final String sMsg)
	{
		runOnUiThread(new Runnable(){
			public void run() 
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(ConfigurationDlg.this);
				AlertDialog alert = builder.create();
				if(sTitle != null)
					alert.setTitle(sTitle);
				else
					alert.setTitle("Error");

				alert.setMessage(sMsg);
				alert.setCancelable(true);
				alert.setButton(DialogInterface.BUTTON_POSITIVE, getString(android.R.string.ok), new DialogInterface.OnClickListener() {public void onClick(DialogInterface dialog, int which) {}});
				alert.show();
			}
		});													
	}
}
