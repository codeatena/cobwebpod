package com.cobwebb.mobile.pod.collector;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;


import com.cobwebb.mobile.pod.collector.UploadActivity.UploadResponseReceiver;
import com.cobwebb.mobile.pod.collector.database.Configuration;
import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;
import com.cobwebb.mobile.pod.collector.database.POD;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class UploadService extends IntentService {
	private static final String TAG = "UploadService";


	public static final String PARM_POD_ID = "PARM_POD_ID";

	public UploadService() {
		super("UploadService");
	}
	
	public UploadService(String name){
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle b = intent.getExtras();
		int podID = b.getInt(UploadService.PARM_POD_ID, -1);
		if(podID < 0)
		{
			Log.e(TAG, "onHandleIntent, invalid podID");
			return;
		}
		
		UploadResponse uploadResponse = new UploadResponse();
		boolean bRet = false;

		try{
			DatabaseHandler db = new DatabaseHandler(this);
			POD pod = db.getPOD(podID);
			
			bRet = uploadMultiPOD(pod, uploadResponse, db);
		}
	    catch (Exception e) 
	    {
	    	uploadResponse.code = -1;
	    	uploadResponse.text = "Exception";
	    	uploadResponse.details = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "ERROR: Upload file to server: " + e.getMessage(), e);
		}

		Intent result = new Intent(UploadResponseReceiver.UPLOAD_PROCESSED);
		result.putExtra(UploadResponseReceiver.PARM_RESULT, bRet);
		result.putExtra(UploadResponseReceiver.PARM_POD_ID, podID);
		result.putExtra(UploadResponseReceiver.PARM_UPLOAD_RESPONSE, uploadResponse);
		
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.sendBroadcast(result);

	}
	
	public boolean uploadMultiPOD(POD pod, UploadResponse uploadResponse, DatabaseHandler db)
	//nResponseCode, String responseText, String responseDetails)
	{	
		String sPODData = pod.getBarcodeData();
		String[] data = sPODData.split("\\" + POD.DATA_SEPARATOR);
		for(int nIndex=0; nIndex < data.length; nIndex++)
		{
			POD singlePOD = new POD();
			singlePOD.setBarcodeData(data[nIndex].trim());
			singlePOD.setSignatureImageUrl(pod.getSignatureImageUrl());
			singlePOD.setSigneeName(pod.getSigneeName());
			if(uploadPOD(singlePOD, uploadResponse, db) == false)
				return(false);
		}
		
		return(true);
	}

	//coud use HttpPost & HttpClient to upload here, not sure why goodcoresoft did it this way
	//(eg: http://toolongdidntread.com/android/android-multipart-post-with-progress-bar/)
	public boolean uploadPOD(POD pod, UploadResponse uploadResponse, DatabaseHandler db)
	//nResponseCode, String responseText, String responseDetails)
	{
		boolean bRet = false;
		uploadResponse.code = 0;
		uploadResponse.text = "";
		uploadResponse.details = "";

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(pod.getSignatureImageUrl());
		if (!sourceFile.isFile()) 
		{
			Log.e(TAG, "ERROR: uploadFile, Source File Does not exist");
			return(false);
		}

		try 
		{
			Configuration config = db.getConfiguration();

			//Create the basic url
			String uploadURL = "";
			if(config.getUploadURL().startsWith("http://") == false)
				uploadURL = "http://";
			uploadURL += config.getUploadURL();

			if(uploadURL.endsWith("/") == false)
				uploadURL += "/";
			//uploadURL += field.getDocstore();


			// open a URL connection to the Servlet
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(uploadURL);

			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();

			// Put the authentication details in the request
			if (config.getUserName() != null) 
			{
				String usernamePassword = config.getUserName() + ":" + config.getPassword();
				String encodedUsernamePassword = Utils.base64Encode(usernamePassword);
				conn.setRequestProperty("Authorization", "Basic " + encodedUsernamePassword);
			}
			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Dont use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(5 * 1000);//wait ten seconds before timing out...

			// conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			dos = new DataOutputStream(conn.getOutputStream());
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "IVSTORE2", "ExitProgramName"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "COBEXIT", "ExitProgramLib"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "IVSTORE2", "JobDescription"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "COBEXIT", "JobDescriptionLib"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "TRUE", "SubmitCommand"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, ",", "Delimiter"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "PNG", "Extension"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "TRUE", "CreateUniqueFilename"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "8", "UniqueFilenameMaxLen"));//Because our file ends up on QDLS

			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getApplicationArea(), "PARM1"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getFolioSubject(), "PARM2"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getDocumentType(), "PARM3"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getComponentType(), "PARM4"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getFolioText(), "PARM5"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, pod.getSigneeName(), "PARM6"));		//Put signee name in Document Text Comments
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, pod.getBarcodeData(), "PARM7"));	//Put barcode data in UserKey
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, "CPPD/IVTEMP", "PARM8"));
			dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getImageViewLibrary(), "PARM9"));

			//dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, pod.getName(), config.getSigneeName()));
			//dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, config.getDriverName(), config.getDriverId()));
			//dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, field.getDocstore(), "Docstore"));
			//dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, field.getDoctype(), "docType"));
			//dos.writeBytes(Utils.attachContent(lineEnd, twoHyphens, boundary, pod.getBarcodeData(), config.getBarcode()));

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"File\";filename=\"" + sourceFile.getName() + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			// create a buffer of maximum size
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);

			while (bytesRead > 0) 
			{
				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			// send multi-part form data necessary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			uploadResponse.code = conn.getResponseCode();
			if(uploadResponse.code == 200)
				bRet = true;
			else
			{
				uploadResponse.text = conn.getResponseMessage();;
				uploadResponse.details = Utils.parseChttpdResponse(conn.getErrorStream());
			}

			// close the streams
			fileInputStream.close();
			dos.flush();
			dos.close();
		} 
		catch (Exception e) 
		{
			uploadResponse.code = -1;
			uploadResponse.text = "Exception";
			uploadResponse.details = e.getMessage();
			e.printStackTrace();
			Log.e(TAG, "ERROR: Upload file to server: " + e.getMessage(), e);
		}
		return bRet;
	}

}
