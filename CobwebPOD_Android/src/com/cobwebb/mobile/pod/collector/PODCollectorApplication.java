package com.cobwebb.mobile.pod.collector;

import android.app.Application;

public class PODCollectorApplication extends Application {
	private static PODCollectorApplication singleton;
	
	public static PODCollectorApplication getInstance(){
		return(singleton);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
	}
	
	
}
