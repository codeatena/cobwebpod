package com.cobwebb.mobile.pod.collector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.http.client.HttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.cobwebb.mobile.pod.collector.database.Configuration;
import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;

import android.content.Context;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;


public class LicenseData {
	private static final String TAG = "LicenseData";
    private static final String MACHINE_ID_FILENAME = "MID";
    private static final String LICENSE_FILENAME = "license.xml";
	private static final String PRODUCT_ID = "cobPOD";
	private static final String LICENSE_SERVER_URL = "http://www.cobwebb.co.uk/app/licenseServer/";
	private static final long LICENSE_FILE_REFRESH_INTERVAL_MSEC = 86400000; //24hours x 60minutes x 60seconds x 1000milliseconds;
	private static String m_sActualMachineID = null;
	private static PublicKey m_publicKey = null;
 	private static ConnectionDetector m_cd = null;
	private static DatabaseHandler m_db = null;
    private static String m_sError = "";
    private static String m_sAppVersion = "";

	private String m_sMachineID = "";
    private String m_sProductID = "";
    private String m_sProductVersion = "";
    private String m_sLicense = "";
    private String m_sLicenseType = "";
    private String m_sExpiryDate = "";
    private String m_sCompanyName = "";
    private String m_sMachineName = "";
    private String m_sExtraInfo = "";

    public synchronized static LicenseData runLicenseCheck(Context context){
    	
      	//Make sure we're initialised
     	init(context);
    	
    	LicenseData dataCur = new LicenseData();

    	//Load existing license from file
    	Log.d(TAG, "Load existing license");
     	Document xmlLicense = loadLocalLicenseXML(context);


    	//If not loaded, try and get from server
    	if(xmlLicense == null)
    	{
    		Log.d(TAG, "requestLicenseData");
    		dataCur.resetData();
    		xmlLicense = dataCur.requestLicenseXML();
    	}
    	
    	if(xmlLicense != null)
    	{
    		Log.d(TAG, "setLicenseData");
    		dataCur.setLicenseData(xmlLicense);
    	}
    	
    	//Check the license
    	Log.d(TAG, "checkLicense");
    	boolean bUpdateLicenseFile = true;
    	boolean bValidLicense = dataCur.checkLicense(m_sActualMachineID, PRODUCT_ID);
    	if(bValidLicense == false)
    	{
    		Log.d(TAG, "invalid license");

    		//Invalid license - reset dataCur and show license dialog
    		dataCur.resetData();
    	}
    	else
    	{

    		//valid license, don't bother the server if we've done so recently
    		if(isLicenseFileStale(context) == false)
    			bUpdateLicenseFile = false;//The only time we won't be bothering the server is if we have a valid license and it isn't stale.

    		Log.d(TAG, "valid license " + (bUpdateLicenseFile ? "STALE" : "FRESH"));
    	}


    	//Update the license
    	if(bUpdateLicenseFile == true)
    	{
    		Log.d(TAG, "bUpdateLicenseFile");

    		//request License from server
    		xmlLicense = dataCur.requestLicenseXML();
    		if(xmlLicense == null)
    		{
    			setLastError("Failed to retrieve license from server");
    		}
    		else
    		{
    			LicenseData dataNew = new LicenseData();
    			dataNew.setLicenseData(xmlLicense);
    			if(dataNew.compareMachineID(m_sActualMachineID) == true)
    			{
    				if(dataNew.compareProductID(PRODUCT_ID) == true)
    				{
    					Log.d(TAG, "saveLicense");
    					//Save license
    					saveLocalLicenseXML(xmlLicense, context);
    					dataCur.setLicenseData(xmlLicense);
    				}
    			}		
    		}
    	}

    	Log.d(TAG, "getLicenseStatus");
	   	return(dataCur);
	}


	private static void init(Context context) {
		if (m_sActualMachineID == null ||  m_sActualMachineID.length() == 0) {  
			File machineIDFile = new File(context.getFilesDir(), MACHINE_ID_FILENAME);
			try {
				if (!machineIDFile.exists())
					writeMachineID(machineIDFile);
				m_sActualMachineID = readMachineID(machineIDFile);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		m_sAppVersion = Utils.getVersionString(context);

		try {
			if(m_publicKey == null)
			{
				// converts the String to a PublicKey instance
				final String keyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXH+Img4EvC7HCfKh82Hbt078BTrJhyZSBIFSXRVQUmB4CiA+ALKAYAaVb86dpPDES1a1FMmLVI0+ZlLqsBnIL1ZvRIL3ZrgKrRPnTuS1XqXOtkH2TSX9T1pQTkrT++s4/EBxw3nrxblxI3xkPTRp9+GT5\niHbag1fWCshfzk4JZwIDAQAB";
				byte[] keyBytes = Base64.decode(keyString.getBytes("UTF-8"), Base64.DEFAULT);
				X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				m_publicKey = keyFactory.generatePublic(spec);
			}
		} catch (Exception e) {
			setLastError("Error creating public key", e);
		}
	
		if(m_cd == null)
			m_cd = new ConnectionDetector(context.getApplicationContext());
	
		if(m_db == null)
			m_db = new DatabaseHandler(context);
	}

    
	private static String readMachineID(File installation) throws IOException {
        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeMachineID(File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        String id = UUID.randomUUID().toString();
        out.write(id.getBytes());
        out.close();
    }

    public static String decryptBase64ToString(String sMessage) throws Exception {
    	byte[] bytesEncoded = sMessage.getBytes("UTF-8");
    	byte[] bytesEncrypted = Base64.decode(bytesEncoded, Base64.DEFAULT);
    	byte[] bytes = blockCipher(bytesEncrypted, Cipher.DECRYPT_MODE);
		String sRet = new String(bytes, "UTF-8");
		return sRet.trim();
    }
  
    public static String encryptStringToBase64(String sMessage) throws Exception {
    	byte[] bytes = sMessage.getBytes("UTF-8");
    	byte[] bytesEncrypted = blockCipher(bytes, Cipher.ENCRYPT_MODE);
    	byte[] bytesEncoded = Base64.encode(bytesEncrypted, Base64.DEFAULT);
		String sRet = new String(bytesEncoded, "UTF-8");
		return sRet;
    }

    private static byte[] blockCipher(byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException{
    	// string initialize 2 buffers.
    	// scrambled will hold intermediate results
    	byte[] scrambled = new byte[0];

    	// toReturn will hold the total result
    	byte[] toReturn = new byte[0];

    	Cipher cipher;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    	cipher.init(mode, m_publicKey);
		} catch (Exception e) {
			setLastError("Error Creating Cipher", e);
			return(null);
		}

    	int maxBlockLength = 128;
    	if(mode == Cipher.ENCRYPT_MODE)
    	{
    		maxBlockLength -= 11;

    	}

    	// another buffer. this one will hold the bytes that have to be modified in this step
    	byte[] buffer = new byte[maxBlockLength];
    	boolean bHaveData = false;//"RSA/ECB/PKCS1Padding" doesn't like being given a buffer full of 0x00
    	
    	for (int i=0; i< bytes.length; i++){

    		// if we filled our buffer array we have our block ready for de- or encryption
    		if ((i > 0) && (i % maxBlockLength == 0)){
 
    	    	if(bHaveData == true)
    	    	{
	    			//execute the operation
	    			scrambled = cipher.doFinal(buffer);
	    			// add the result to our total result.
	    			toReturn = append(toReturn,scrambled);
    	    	}
    			// here we calculate the length of the next buffer required
    			int newlength = maxBlockLength;

    			// if newlength would be longer than remaining bytes in the bytes array we shorten it.
    			if (i + maxBlockLength > bytes.length) {
    				newlength = bytes.length - i;
    			}
    			// clean the buffer array
    			buffer = new byte[newlength];
    			bHaveData = false;
    		}
    		// copy byte into our buffer.
    		buffer[i%maxBlockLength] = bytes[i];
    		if(bytes[i] != 0x00)
    			bHaveData = true;
    	}

    	// this step is needed if we had a trailing buffer. should only happen when encrypting.
    	// example: we encrypt 110 bytes. 100 bytes per run means we "forgot" the last 10 bytes. they are in the buffer array
    	if(bHaveData == true)
    	{
    		scrambled = cipher.doFinal(buffer);
    		// final step before we can return the modified data.
    		toReturn = append(toReturn,scrambled);
    	}

    	return toReturn;
    }
    private static byte[] append(byte[] prefix, byte[] suffix){
    	byte[] toReturn = new byte[prefix.length + suffix.length];
    	for (int i=0; i< prefix.length; i++){
    		toReturn[i] = prefix[i];
    	}
    	for (int i=0; i< suffix.length; i++){
    		toReturn[i+prefix.length] = suffix[i];
    	}
    	return toReturn;
    }    
    
    private static Document loadLocalLicenseXML(Context context) {
    	Document document = null;
		try
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			document = db.parse(new File(context.getFilesDir(), LICENSE_FILENAME));
		}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			Log.e(TAG, "loadLicenseXML Exception : " + e.getMessage(), e);
		}
		return(document);
	}

    private static boolean saveLocalLicenseXML(Document xmlLicense, Context context) {
    	boolean bRet = false;
 		try
		{
		   	TransformerFactory factory = TransformerFactory.newInstance();
		   	if (factory.getFeature(StreamResult.FEATURE)) {
		    	Transformer transformer = factory.newTransformer();
		    	DOMSource domSource = new DOMSource(xmlLicense);
		    	StreamResult result = new StreamResult(new File(context.getFilesDir(), LICENSE_FILENAME));
		    	transformer.transform(domSource, result);
		    	bRet = true;
		   	}
		}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			Log.e(TAG, "saveLicenseXML Exception : " + e.getMessage(), e);
		}
		return(bRet);
	}
    
    private boolean setLicenseData(Document licenseData){

    	boolean bRet = false;
    	try {
        	XPath xpath = XPathFactory.newInstance().newXPath();
        	
        	m_sMachineID	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/machine_id", licenseData, XPathConstants.NODE));
        	m_sProductID	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/product_id", licenseData, XPathConstants.NODE));
        	m_sLicense		= unSignNode((Node) xpath.evaluate("/LicenseServer/key/license", licenseData, XPathConstants.NODE));
        	m_sLicenseType	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/license_type", licenseData, XPathConstants.NODE));
        	m_sExpiryDate	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/expiry_date", licenseData, XPathConstants.NODE));
        	m_sCompanyName	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/company_name", licenseData, XPathConstants.NODE));
        	m_sMachineName	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/machine_name", licenseData, XPathConstants.NODE));
        	m_sExtraInfo	= unSignNode((Node) xpath.evaluate("/LicenseServer/key/extra_info", licenseData, XPathConstants.NODE));
        	bRet = true;
    	}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			setLastError("setLicenseData Exception : " + e.getMessage(), e);
		}
    	return(bRet);
    }

    public String getLicenseStatus(){
    	String sRet = new String();
    	if(m_sLicenseType.equals("DISABLED"))
    	{
    		sRet = "Disabled";
    	}
    	else if(isLicenseActive() == true)
    	{
    		sRet = "Licensed";
    	}
    	else
    	{
    		sRet = "Unlicensed";
    	}

    	if(m_sLicenseType.equals("TEMPORARY"))
    	{
    		sRet += (isLicenseActive() == true) ? " (Expires: " : " (Expired: "; 
    		sRet += getExpiry().format("%x");
    		sRet += ")";
    	}
    	return(sRet);
    }

    private boolean compareMachineID(String sID){
    	return(m_sMachineID.equals(sID));
    }

    private boolean compareProductID(String sID){
    	return(m_sProductID.equals(sID));
    }

    private boolean checkLicense(String sMachineID, String sProductID){
    	if(compareMachineID(sMachineID) == false)
    		return(false);

    	if(compareProductID(sProductID) == false)
    		return(false);

    	if(m_sLicenseType.equals("PERMANENT"))
    		return(true);

    	if(m_sLicenseType.equals("TEMPORARY") && (isLicenseActive() == true))
    		return(true);

    	return(false);
    }
    
    public boolean isLicenseActive(){

    	if(m_sLicenseType.equals("PERMANENT"))
    		return(true);//Always active

    	if(m_sExpiryDate.length() != 8)
    		return(false);

    	Time now = new Time();
    	now.setToNow();
    	
    	Time licenseExpiry = getExpiry();

    	if(now.after(licenseExpiry))
    		return(false);

    	return(true);
    }

    Time getExpiry(){
    	int year	= Integer.parseInt(m_sExpiryDate.substring(0, 4));
    	int month	= Integer.parseInt(m_sExpiryDate.substring(4, 6));
    	int day		= Integer.parseInt(m_sExpiryDate.substring(6, 8));
    	if(year < 1970) year = 1970;
    	if(month < 1 || month > 12) month = 1;
    	if(day < 1 || day > 31) day = 1;

    	Time licenseExpiry = new Time();
    	licenseExpiry.set(59, 59, 23, day, month, year);
    	return(licenseExpiry);
    }
    
    private String unSignNode(Node node)  throws Exception {
    	String sNodeText = node.getTextContent();
    	String sRet = decryptBase64ToString(sNodeText);
    	return(sRet);
    }
    private static void setLastError(String sError){
    	m_sError = sError;
    	Log.e(TAG, m_sError);
    	
    }
    private static void setLastError(String sError, Throwable e){
    	m_sError = sError;
    	Log.e(TAG, m_sError, e);
    }
    public static String getLastError(){
    	return(m_sError);
    }

    private Document  requestLicenseXML(){
    	if (m_cd.isConnectingToInternet() == false) 
    	{
			setLastError("No network connection");
			return(null);
    	}
 
    	Document xml = null;
    	try{
	    	HttpClient httpClient = new DefaultHttpClient();
	        HttpPost request = new HttpPost(LICENSE_SERVER_URL);
	        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	    	if(m_sMachineID.length() > 0)
	            reqEntity.addPart("machine_id", new StringBody(encryptStringToBase64(m_sMachineID)));
	    	if(m_sProductID.length() > 0)
	            reqEntity.addPart("product_id", new StringBody(encryptStringToBase64(m_sProductID)));
	    	if(m_sAppVersion.length() > 0)
	            reqEntity.addPart("product_version", new StringBody(encryptStringToBase64(m_sAppVersion)));
	    	if(m_sLicense.length() > 0)
	            reqEntity.addPart("license", new StringBody(encryptStringToBase64(m_sLicense)));
	    	if(m_sLicenseType.length() > 0)
	            reqEntity.addPart("license_type", new StringBody(encryptStringToBase64(m_sLicenseType)));
	    	if(m_sExpiryDate.length() > 0)
	            reqEntity.addPart("expiry_date", new StringBody(encryptStringToBase64(m_sExpiryDate)));
	    	
			Configuration config = m_db.getConfiguration();
			m_sMachineName = config.getDriverId();
			m_sCompanyName = config.getCompanyName();
	    	if(m_sMachineName.length() > 0)
	            reqEntity.addPart("machine_name", new StringBody(encryptStringToBase64(m_sMachineName)));
	    	if(m_sCompanyName.length() > 0)
	            reqEntity.addPart("company_name", new StringBody(encryptStringToBase64(m_sCompanyName)));
	    	if(m_sExtraInfo.length() > 0)
	            reqEntity.addPart("extra_info", new StringBody(encryptStringToBase64(m_sExtraInfo)));
	        
	        request.setEntity(reqEntity);       
	        HttpResponse response = httpClient.execute(request);
	        
			// Responses from the server (code and message)
			if(response.getStatusLine().getStatusCode() == 200)
			{
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				xml = db.parse(response.getEntity().getContent());
			}
			else
			{
				HttpEntity entity = response.getEntity();
				String sError = EntityUtils.toString(entity);
				Log.d(TAG, "Error reading license: " + sError);
			}
    	}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			setLastError("requestLicenseXML Exception : " + e.getMessage(), e);
		}

		return(xml);
    }

    private static boolean isLicenseFileStale(Context context){
    	boolean bStale = true;
    	
    	try{
            File licenseFile = new File(context.getFilesDir(), LICENSE_FILENAME);
            long lFileMod = licenseFile.lastModified();//Returns the time when this file was last modified, measured in milliseconds since January 1st, 1970, midnight. Returns 0 if the file does not exist.
            if(lFileMod > 0)
            {
                long lCur = System.currentTimeMillis();//Returns the current system time in milliseconds since January 1, 1970 00:00:00 UTC.

            	if((lFileMod + LICENSE_FILE_REFRESH_INTERVAL_MSEC) > lCur)
            		bStale = false;//Fresh
            }
    	}
	    catch(Exception e) 
	    {
			e.printStackTrace();
			setLastError("isLicenseFileStale Exception : " + e.getMessage(), e);
		}
  	
    	return(bStale);
    }
    
	// getting Machine ID
	public String getMachineID() {
		return this.m_sMachineID;
	}

	// setting Machine ID
	public void setMachineID(String sMachineID) {
		this.m_sMachineID = sMachineID;
	}

	// getting Product ID
	public String getProductID() {
		return this.m_sProductID;
	}

	// setting Product ID
	public void setProductID(String sProductID) {
		this.m_sProductID = sProductID;
	}
	
	// setting Extra Info
	public void setExtraInfo(String sExtraInfo) {
		this.m_sExtraInfo = sExtraInfo;
	}

	// setting Product ID
	public void resetData() {
		this.setMachineID(m_sActualMachineID);
		this.setProductID(PRODUCT_ID);
	}

}
