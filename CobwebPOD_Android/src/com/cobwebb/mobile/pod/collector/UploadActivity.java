package com.cobwebb.mobile.pod.collector;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.cobwebb.mobile.pod.collector.database.DatabaseHandler;
import com.cobwebb.mobile.pod.collector.database.POD;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

public class UploadActivity extends Activity {
	private static final String TAG = "UploadActivity";
	
	private ListView m_ProgressView;
	private final ArrayList<BasicNameValuePair> m_ProgressList = new ArrayList<BasicNameValuePair>(); 
	ArrayAdapter<BasicNameValuePair> m_progressAdapter;
	
	private TextView 		m_TitleText;
	private Button 			m_btnAction;
	private DatabaseHandler m_db;
	private List<POD> 		m_pods;
	private int 			m_podsUploaded;
	
	public class UploadResponseReceiver extends BroadcastReceiver {
	   public static final String UPLOAD_PROCESSED = "com.cobwebb.intent.action.UPLOAD_PROCESSED";
	   public static final String PARM_RESULT = "PARM_RESULT";
	   public static final String PARM_POD_ID = "PARM_POD_ID";
	   public static final String PARM_UPLOAD_RESPONSE = "PARM_UPLOAD_RESPONSE";
	   @Override
	    public void onReceive(Context context, Intent intent) {
	       int podID = intent.getIntExtra(PARM_POD_ID, -1);
	       if(podID < 0)
	       {
	    	   Log.e(TAG, "ERROR, onReceive invalid podID");
	    	   return;
	       }
	       
	       boolean bRet = intent.getBooleanExtra(PARM_RESULT, false);
	       UploadResponse uploadResponse = intent.getParcelableExtra(PARM_UPLOAD_RESPONSE);
	       POD pod = m_db.getPOD(podID);
    	   String sComment = String.format(Locale.ENGLISH, "Delivery: %s\nSignee: %s", pod.getBarcodeData(), pod.getSigneeName());
	       if(bRet == true)
	       {
		   		File file = new File(pod.getSignatureImageUrl());
				file.delete();
				pod.setIsUploaded(1);
				m_db.updatePOD(pod);
	    	   addProgressText("POD " + podID + " Uploaded", sComment);
	    	   m_podsUploaded++;
	       }
	       else
	       {
	    	   sComment += String.format(Locale.ENGLISH, "\nError code: %d\nError text: %s\n%s", uploadResponse.code, uploadResponse.text, uploadResponse.details);
	    	   addProgressText("POD " + podID + " Not Uploaded", sComment);
	       }

	       uploadNextPOD();
	    }
	}
	private UploadResponseReceiver uploadResponseReceiver = new UploadResponseReceiver();
	
	
	public class LicenseCheckReceiver extends BroadcastReceiver {
		public static final String LICENSE_CHECKED = "com.cobwebb.intent.action.LICENSE_CHECKED";
		public static final String PARM_LICENSE_CHECK_RESULT 	= "PARM_LICENSE_CHECK_RESULT";
		public static final String PARM_LICENSE_STATUS 			= "PARM_LICENSE_STATUS";
		public static final String PARM_LICENSE_ERROR 			= "PARM_LICENSE_ERROR";
		@Override
		public void onReceive(Context context, Intent intent) {
			boolean bLicensed = intent.getBooleanExtra(PARM_LICENSE_CHECK_RESULT, false);
			String sLicense = intent.getStringExtra(PARM_LICENSE_STATUS);
			if(bLicensed == true)
			{
				addProgressText("License Checked", sLicense);
				uploadNextPOD();
			}
			else
			{
				String sError = intent.getStringExtra(PARM_LICENSE_ERROR);
				
				String sComment = getString(R.string.license_invalid_dlg_message);
				if(sLicense.length() > 0)
					sComment += "\nLicense: " + sLicense;

				AlertDialog.Builder alert = new AlertDialog.Builder(UploadActivity.this);
				alert.setTitle(R.string.license_invalid_dlg_title);
				if(sLicense.equals("Disabled") == false)
				{
					sComment += "\n" + getString(R.string.license_invalid_dlg_continue_msg);
					alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							//Carry on and upload
							uploadNextPOD();
						} 
					}); 
				}
				else
				{
					m_btnAction.setText(getString(R.string.done));
					alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						} 
					}); 
				
				}
				alert.setMessage(sComment);
				alert.show();

				if(sError.length() > 0)
					sComment += "\nError: " + sError;
				addProgressText("License Check", sComment);
			}
		}
	}
	private LicenseCheckReceiver licenseCheckReceiver = new LicenseCheckReceiver();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload);

		m_TitleText = (TextView) findViewById(R.id.uploadTitleText);
		m_ProgressView = (ListView) findViewById(R.id.progressText);
		m_btnAction	= (Button) findViewById(R.id.action);
		
		m_progressAdapter = new ArrayAdapter<BasicNameValuePair>(this, android.R.layout.simple_list_item_2, m_ProgressList){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
	            TwoLineListItem row;
	            if(convertView == null){
	                LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	                row = (TwoLineListItem)inflater.inflate(android.R.layout.simple_list_item_2, null);
	            }else{
	                row = (TwoLineListItem)convertView;
	            }

	            BasicNameValuePair data = m_ProgressList.get(position);
	            row.getText1().setText(data.getName());
	            row.getText2().setText(data.getValue());

	            Theme theme = getTheme();
	            TypedValue tv = new TypedValue();
	            if(theme.resolveAttribute(android.R.attr.textColorPrimary, tv, true) == true)
	            	row.getText1().setTextColor(getResources().getColor(tv.resourceId));
	            else if(theme.resolveAttribute(android.R.attr.color, tv, true) == true)
	            	row.getText1().setTextColor(getResources().getColor(tv.resourceId));

	            //if(theme.resolveAttribute(android.R.attr.textColorSecondary, tv, true) == true)
	            	//row.getText2().setTextColor(getResources().getColor(tv.resourceId));
	            //else if(theme.resolveAttribute(android.R.attr.color, tv, true) == true)
	            	//row.getText2().setTextColor(getResources().getColor(tv.resourceId));

	            //row.getText1().setTextColor(android.R.attr.textColorPrimary);
	            //row.getText2().setTextColor(android.R.attr.colorForeground);

	            return row;
	        }
			
	    };
		m_ProgressView.setAdapter(m_progressAdapter);
			//-------------------------------------------------------------------------------
		// Cancel / Done
		m_btnAction.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = getIntent();
				setResult(RESULT_OK,  i);
				finish();
				/*
				if(m_pods.isEmpty())
					finish();
				else
					m_pods.clear();
				*/
			}
		});


		//	Submit all the confirmations	
		m_db = new DatabaseHandler(this);
		m_pods = m_db.getPendingPODs();//get all Confirmations that haven't been uploaded

		if(m_pods.isEmpty())
		{
			addProgressText("No Confirmations to Upload", "");
			m_btnAction.setText(getString(R.string.done));
			return;			
		}

		////////////////////////////////////////////////////////////
		//Validating the License
		
		m_TitleText.setText("Validating License");
		Intent LicenseCheckIntent = new Intent(this, LicenseCheckService.class);
		startService(LicenseCheckIntent);
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();

		EasyTracker easyTracker = EasyTracker.getInstance(this);
		easyTracker.send(MapBuilder
			      .createEvent("process",    // Event category (required)
		                   		"pod_upload",  		// Event action (required)
		                   		"upload_count",   	// Event label
		                   		Long.valueOf(m_podsUploaded))     // Event value
		      .build()
		  );

		easyTracker.activityStop(this);  // Add this method.
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.registerReceiver(uploadResponseReceiver, new IntentFilter(UploadResponseReceiver.UPLOAD_PROCESSED));
		lbm.registerReceiver(licenseCheckReceiver, 	new IntentFilter(LicenseCheckReceiver.LICENSE_CHECKED));
	}

	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
		lbm.unregisterReceiver(uploadResponseReceiver);
		lbm.unregisterReceiver(licenseCheckReceiver);
	}

	protected void uploadNextPOD() {

		switch(m_pods.size())
		{
			case 0:
				m_TitleText.setText("Uploading complete");
				m_btnAction.setText(getString(R.string.done));
				return;
			case 1:
				m_TitleText.setText("Uploading 1 Confirmation");
				break;
			default:
				m_TitleText.setText("Uploading " + m_pods.size() + " Confirmations");
				break;
		}

		POD pod = m_pods.remove(0);
		Intent uploadIntent = new Intent(this, UploadService.class);
		uploadIntent.putExtra(UploadService.PARM_POD_ID, pod.getID());
		startService(uploadIntent);
	}

	protected void addProgressText(String progressText, String progressComment) {
		BasicNameValuePair txt = new BasicNameValuePair(progressText, progressComment);
		m_ProgressList.add(txt);
		m_progressAdapter.notifyDataSetChanged();
	}

}
