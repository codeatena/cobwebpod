package com.cobwebb.mobile.pod.collector;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SigneeCapture extends Activity {
	
	Button mBack, mSave;
	private EditText yourName;
	private TextView txtViewBarcode;
	public static final int SIGNATURE_ACTIVITY = 110;
	Bundle m_bundle;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		m_bundle = getIntent().getExtras();		
		
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.signee);
			
		yourName = (EditText) findViewById(R.id.signeeName);
		mSave = (Button) findViewById(R.id.save);
		mBack = (Button) findViewById(R.id.back);
		txtViewBarcode = (TextView) findViewById(R.id.barcode);
		txtViewBarcode.setText(m_bundle.getString("barcode"));
			
		mSave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Signeer Captured Saved");	
				Intent i = getIntent();
				if (yourName.getText().toString().trim().equalsIgnoreCase("")) 
				{														
					Toast toast = Toast.makeText(SigneeCapture.this, "Please enter your Name", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.TOP, 105, 50);
					toast.show();
				}
				else
				{
					//DatabaseHandler db = new DatabaseHandler(SigneeCapture.this);
					
					String signeeName = yourName.getText().toString();
					
					Bundle b = new Bundle();
					b.putString("signeeName", signeeName);
					String barcode = m_bundle.getString("barcode"); 
					b.putString("barcode", barcode);
					
															
				 	Intent intent = new Intent(SigneeCapture.this, SignatureCapture.class);
				 	intent.putExtras(b);
				 	startActivityForResult(intent, SIGNATURE_ACTIVITY);
				 	
				}
			}
		});			
		
		mBack.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.v("log_tag", "Signee Captured Canceled");				
				setResult(RESULT_CANCELED,  getIntent());
				finish();
			}
		});

	}

	@Override
	protected void onDestroy() {
		Log.w("GetSgnee", "onDestory");
		super.onDestroy();
	}	

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}


	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{
		if (requestCode == SIGNATURE_ACTIVITY) 
		{
			//Forward the result back up the Activity chain
			setResult(resultCode, intent);
			finish();
		} 
	}
}



